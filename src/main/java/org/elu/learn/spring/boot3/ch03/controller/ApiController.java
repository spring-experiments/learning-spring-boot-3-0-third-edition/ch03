package org.elu.learn.spring.boot3.ch03.controller;

import org.elu.learn.spring.boot3.ch03.model.NewVideo;
import org.elu.learn.spring.boot3.ch03.model.VideoEntity;
import org.elu.learn.spring.boot3.ch03.service.VideoService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/videos")
public class ApiController {
    private final VideoService videoService;

    public ApiController(VideoService videoService) {
        this.videoService = videoService;
    }

    @GetMapping
    public List<VideoEntity> all() {
        return videoService.getVideos();
    }

    @PostMapping
    public VideoEntity newVideo(@RequestBody NewVideo newVideo) {
        return videoService.create(newVideo);
    }
}
