package org.elu.learn.spring.boot3.ch03.model;

public record VideoSearch(String name, String description) {
}
