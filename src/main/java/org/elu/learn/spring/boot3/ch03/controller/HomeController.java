package org.elu.learn.spring.boot3.ch03.controller;

import org.elu.learn.spring.boot3.ch03.model.UniversalSearch;
import org.elu.learn.spring.boot3.ch03.model.NewVideo;
import org.elu.learn.spring.boot3.ch03.model.VideoEntity;
import org.elu.learn.spring.boot3.ch03.model.VideoSearch;
import org.elu.learn.spring.boot3.ch03.service.VideoService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class HomeController {
    private final VideoService videoService;

    public HomeController(VideoService videoService) {
        this.videoService = videoService;
    }

    @GetMapping("/")
    public String index(Model model) {
        model.addAttribute("videos", videoService.getVideos());
        return "index";
    }

    @PostMapping("/new-video")
    public String newVideo(@ModelAttribute NewVideo newVideo) {
        videoService.create(newVideo);
        return "redirect:/";
    }

    @PostMapping("/multi-field-search")
    public String multiFieldSearch(@ModelAttribute VideoSearch search, Model model) {
        List<VideoEntity> searchResult = videoService.search(search);
        model.addAttribute("videos", searchResult);
        return "index";
    }

    @PostMapping("/universal-search")
    public String universalSearch(@ModelAttribute UniversalSearch search, Model model) {
        List<VideoEntity> searchResult =  videoService.search(search);
        model.addAttribute("videos", searchResult);
        return "index";
    }
}
