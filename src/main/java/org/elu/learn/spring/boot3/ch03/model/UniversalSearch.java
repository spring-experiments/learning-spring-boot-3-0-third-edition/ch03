package org.elu.learn.spring.boot3.ch03.model;

public record UniversalSearch(String value) {
}
