package org.elu.learn.spring.boot3.ch03.service;

import jakarta.annotation.PostConstruct;
import org.elu.learn.spring.boot3.ch03.model.NewVideo;
import org.elu.learn.spring.boot3.ch03.model.UniversalSearch;
import org.elu.learn.spring.boot3.ch03.model.VideoEntity;
import org.elu.learn.spring.boot3.ch03.model.VideoSearch;
import org.elu.learn.spring.boot3.ch03.repository.VideoRepository;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class VideoService {
    private final VideoRepository videoRepository;

    public VideoService(VideoRepository videoRepository) {
        this.videoRepository = videoRepository;
    }

    public List<VideoEntity> getVideos() {
        return videoRepository.findAll();
    }

    public VideoEntity create(NewVideo newVideo) {
        return videoRepository.saveAndFlush(new VideoEntity(newVideo.name(), newVideo.description()));
    }

    public List<VideoEntity> search(VideoSearch search) {
        if (StringUtils.hasText(search.name()) &&
            StringUtils.hasText(search.description())) {
            return videoRepository
                .findByNameContainsOrDescriptionContainsAllIgnoreCase(search.name(), search.description());
        }

        if (StringUtils.hasText(search.name())) {
            return videoRepository.findByNameContainsIgnoreCase(search.name());
        }

        if (StringUtils.hasText(search.description())) {
            return videoRepository.findByDescriptionContainsIgnoreCase(search.description());
        }

        return List.of();
    }

    public List<VideoEntity> search(UniversalSearch search) {
        VideoEntity probe = new VideoEntity();
        if (StringUtils.hasText(search.value())) {
            probe.setName(search.value());
            probe.setDescription(search.value());
        }
        Example<VideoEntity> example = Example.of(probe,
                                                  ExampleMatcher.matchingAny()
                                                                .withIgnoreCase()
                                                                .withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING));
        return videoRepository.findAll(example);
    }

    @PostConstruct
    void initDatabase() {
        videoRepository.save(
            new VideoEntity("Need HELP with your SPRING BOOT 3 App?",
                            "SPRING BOOT 3 will only speed things up and make it super SIMPLE to serve templates and raw data."));
        videoRepository.save(
            new VideoEntity("Don't do THIS to your own CODE!",
                            "As a pro developer, never ever EVER do this to your code. Because you'll ultimately be doing it to YOURSELF!"));
        videoRepository.save(
            new VideoEntity("SECRETS to fix BROKEN CODE!",
                            "Discover ways to not only debug your code, but to regain your confidence and get back in the game as a software developer."));
    }
}
