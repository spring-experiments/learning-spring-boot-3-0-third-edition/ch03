package org.elu.learn.spring.boot3.ch03.model;

public record NewVideo(String name, String description) {
}
